---

## Calculate similarity between raw sequence [all_F_fasta] and its reverse-complement
$python2 similarity.py [all_F_fasta] [sim_txt]

## BLAST-based Alignment Loops
1. Subsample your target sequencing files[F_fasta] from raw filtered sequences [all_F_fasta]
$seqkit seq -m {min-len} -M {max-len} -w 0 [all_F_fasta] > [F_fasta]
Note that: set the minimum or maximum of length range based on the project. The output [F_fasta] used for further alignment analysis.
2. Blast the selected subsampled sequence file to the reference genome [ref_fasta]
Example:
$makeblastdb -in ref.fasta -dbtype nucl
$blastn -db ref.fasta -query [F_fasta] -task blastn -outfmt 6 -max_hsps 1 -out [b1]
$python2 LS.py [F_fasta] [b1] [L1-1]
$python2 RS.py [F_fasta] [b1] [L1-2] 
Note that: The number of alignment loop in specific length range based on the project. 
For each loop, get unmatched fragments in the previous alignment of the same HiFi read, on left or right sides for next alignment, in Python script with LS.py and RS.py, respectively.

## Interpret the output of Alignments

1. Place the Blast [b*] output text file from the previous alignments in order, and then visualize detail data of alignment in excel with VBA code.
 "VBA_code in excel"
2. set the filters, conditional formatting the regions in different color.
 such as the ITR: 1-145 or GOI: 647-965 or R-ITR: 3859-4003
3. calculate the ratio of specific rAAV configuration.

---